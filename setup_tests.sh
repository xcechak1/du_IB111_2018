#!/bin/bash

# clone repo
git clone git@gitlab.fi.muni.cz:xcechak1/du_IB111_2018.git

# prepare python environment
cd du_IB111_2018
python3 -m pip install -r requirements.txt --user
