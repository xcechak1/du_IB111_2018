import sys
import os
import importlib

import pytest


def pytest_addoption(parser):
    parser.addoption("--teacher",
                     help="teacher: path to script with teacher solution")
    parser.addoption("--student",
                     help="student: path to script with student solution")


@pytest.fixture(scope="session")
def teacher(request):
    path_to_file = request.config.getoption("--teacher")
    teacher = do_import(path_to_file)
    return teacher


@pytest.fixture(scope="session")
def student(request):
    path_to_file = request.config.getoption("--student")
    student = do_import(path_to_file)
    return student


def do_import(path_to_file):
    sys.path.append(os.path.dirname(path_to_file))
    module_name = os.path.splitext(os.path.basename(path_to_file))[0]
    imported = importlib.import_module(module_name)
    return imported
